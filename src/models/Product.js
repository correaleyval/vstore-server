const  { Schema, model} = require('mongoose')

const Product = new Schema({
    name: String,
    description: String,
    imageURL: String,
    public_id: String,
    date: { type: Date, default: Date.now }
})

module.exports = model('Product', Product)