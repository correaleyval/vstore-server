const express = require('express')
const morgan = require('morgan')
const path = require('path')
require('./database')

const app = express()

app.set('port', process.env.PORT || 3000)

app.use(express.static(path.join(__dirname, '../public')));

app.use(morgan('dev'))

app.use(express.json())

app.use(express.urlencoded({
    extended: true,
    limit: '10mb'
}))

app.use(require('./routes'))

module.exports = app